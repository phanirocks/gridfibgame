let gridSize = 50;
let cellArray = new Array(gridSize);
const container = document.querySelector('.grid');
const resetButton = document.getElementById('resetbutton');
resetButton.addEventListener('click', () => {
    const selectBox = document.getElementById('gridsize');
    const value = selectBox.options[selectBox.selectedIndex].value;
    container.innerHTML = '';
    gridSize = value;
    setGrid();
});

function setGrid() {
    for (let i = 0; i < gridSize; i++) {
        let row = new Array(50);
        for (let j = 0; j < gridSize; j++) {
            const cell = document.createElement("button");
            cell.classList.add('cell');
            cell.classList.add('invisibletext');
            cell.style.width = 100 / gridSize + '%';
            cell.style.textAlign = 'center';
            cell.textContent = 0;
            cell.addEventListener('click', () => {
                for (let k = 0; k < gridSize; k++) {
                    let val = parseInt(cellArray[i][k].textContent);
                    val++;
                    cellArray[i][k].classList.add('transition');
                    cellArray[i][k].classList.remove('invisibletext');
                    cellArray[i][k].classList.add('visibletext');
                    cellArray[i][k].textContent = val;
                    setTimeout(function() {
                        cellArray[i][k].classList.remove('transition');
                    }, 1000);
                    val = parseInt(cellArray[k][j].textContent);
                    val++;
                    cellArray[k][j].classList.add('transition');
                    cellArray[k][j].classList.remove('invisibletext');
                    cellArray[k][j].classList.add('visibletext');
                    cellArray[k][j].textContent = val;
                    setTimeout(function() {
                        cellArray[k][j].classList.remove('transition');
                    }, 1000);
                }
                evaluateRow(i, 0, gridSize);
                setTimeout(function() {
                    for (let p = 0; p < gridSize; p++) {
                        evaluateColumn(p, i - 4, i + 4);
                    }
                }, 1000);

                evaluateColumn(j, 0, gridSize);
                setTimeout(function() {
                    for (let p = 0; p < gridSize; p++) {
                        evaluateRow(p, j - 4, j + 4);
                    }

                }, 1000);
                let val = parseInt(cellArray[i][j].textContent);
                val = val - 1;
                cellArray[i][j].textContent = val;
            })
            row[j] = cell;
            container.append(cell);
        }
        cellArray[i] = row;
    }
}

function getNextFib(current) {
    if (current == 0) {
        return -1;
    }
    let phi = (1 + Math.sqrt(5)) / 2
    let next = Math.round(current * phi)
    return next;
}

function evaluateRow(row, startColumn, endColumn) {
    while (startColumn < 0) {
        startColumn++;
    }
    while (endColumn >= gridSize) {
        endColumn--;
    }
    console.log("row is " + row);
    console.log("startColumn is" + startColumn);
    console.log("endColumn is" + endColumn);
    let i = startColumn + 1;
    let count = 0;
    let prev = -1;
    if (!cellArray[row][startColumn].classList.contains('cleared')) {
        prev = parseInt(cellArray[row][startColumn].textContent);
    }
    let current = -1;
    while (i <= endColumn) {
        if (cellArray[row][i].classList.contains('cleared')) {
            count = 0;
            prev = -1;
            i++;
            continue;
        }
        if (prev === -1) {
            prev = parseInt(cellArray[row][i].textContent);
            current = -1;
            i++;
            continue;
        } else {
            current = parseInt(cellArray[row][i].textContent);
        }
        if ((current === 1 && prev === 1 && count == 0) || current === getNextFib(prev) && current !== -1) {
            if (count === 3) {
                for (let k = 5; k > 0; k--) {
                    cellArray[row][i - k + 1].classList.remove('transition');
                    cellArray[row][i - k + 1].classList.remove('cell');
                    cellArray[row][i - k + 1].classList.add('cleared');
                    cellArray[row][i - k + 1].disabled = true;
                }
                count = 0;
                i++;
                prev = -1;
                continue;
            } else {
                count = count + 1;
            }
        } else {
            count = 0;
        }
        prev = current;
        i++;
    }
}

function evaluateColumn(column, startRow, endRow) {
    while (startRow < 0) {
        startRow++;
    }
    while (endRow >= gridSize) {
        endRow--;
    }
    console.log("column is " + column);
    console.log("startRow is" + startRow);
    console.log("endRow is" + endRow);
    let i = startRow + 1;
    let count = 0;
    let prev = -1;
    if (!cellArray[startRow][column].classList.contains('cleared')) {
        prev = parseInt(cellArray[startRow][column].textContent);
    }
    let current = -1;
    while (i <= endRow) {
        if (cellArray[i][column].classList.contains('cleared')) {
            count = 0;
            prev = -1;
            i++;
            continue;
        }
        if (prev === -1) {
            prev = parseInt(cellArray[i][column].textContent);
            current = -1;
            i++;
            continue;
        } else {
            current = parseInt(cellArray[i][column].textContent);
        }
        if ((current === 1 && prev === 1 && count == 0) || current === getNextFib(prev) && current !== -1) {
            if (count === 3) {
                for (let k = 5; k > 0; k--) {
                    cellArray[i - k + 1][column].classList.remove('transition');
                    cellArray[i - k + 1][column].classList.remove('cell');
                    cellArray[i - k + 1][column].classList.add('cleared');
                    cellArray[i - k + 1][column].disabled = true;
                }
                count = 0;
                i++;
                prev = -1;
                continue;
            } else {
                count = count + 1;
            }
        } else {
            count = 0;
        }
        prev = current;
        i++;
    }
}